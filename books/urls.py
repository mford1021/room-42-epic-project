from django.urls import path, include
from books.views import books

urlpatterns = [
    path("", books, name="books"),
]