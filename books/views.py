from django.shortcuts import render
from books.models import Book

# Create your views here.

def books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)
